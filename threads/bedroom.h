#ifndef BEDROOM_H
#define BEDROOM_H

#include "list.h"
#include "thread.h"

class BedRoom {
private:

  class Bed {
  public:
    Thread *sleeper; // waiting thread
    int wakeTime;    // time to call thread back to execution
    Bed() {sleeper = NULL; wakeTime = 0;}
    Bed(Thread *t, int x):sleeper(t), wakeTime(x) {};
    bool operator== (const Bed& compareTo) {
      return ((sleeper == compareTo.sleeper) && (wakeTime == compareTo.wakeTime));
    }
  };

  List<Bed> *beds;
  int current_interrupt; // timer to count remain time to wait

public:
  BedRoom();
  ~BedRoom();
  // put thread on the waiting queue
  void PutToBed(Thread *t, int x);
  //find threads to wake up
  bool MorningCall();
  // check whether the waiting queue is empty
  bool IsEmpty(){return beds->IsEmpty();};
};

#endif
